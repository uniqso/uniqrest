# == Schema Information
#
# Table name: entities
#
#  id             :integer          not null, primary key
#  entity_type_id :integer
#  name           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  latitude       :float
#  longitude      :float
#  description    :string
#  url            :string
#

FactoryGirl.define do
  factory :entity do
    entity_type_id 1
name "MyString"
address "MyString"
review_id 1
  end

end
