# == Schema Information
#
# Table name: locations
#
#  id            :integer          not null, primary key
#  restaurant_id :integer
#  country       :string
#  city          :string
#  address       :string
#  latitude      :float
#  longitude     :float
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#

class Location < ActiveRecord::Base
  belongs_to :restaurant

  geocoded_by :full_street_address
  after_validation :geocode

  def full_street_address
    [ self.address, self.city, self.country ].join(', ')
  end
end
