# == Schema Information
#
# Table name: entities
#
#  id             :integer          not null, primary key
#  entity_type_id :integer
#  name           :string
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  latitude       :float
#  longitude      :float
#  description    :string
#  url            :string
#

class Entity < ActiveRecord::Base
  belongs_to :entity_type
  has_one :review
end
