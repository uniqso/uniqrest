# == Schema Information
#
# Table name: restaurants
#
#  id          :integer          not null, primary key
#  name        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  latitude    :float
#  longitude   :float
#  description :string
#  url         :string
#

class Restaurant < ActiveRecord::Base
  has_one :location
  has_one :review

  accepts_nested_attributes_for :location
  accepts_nested_attributes_for :review
end
