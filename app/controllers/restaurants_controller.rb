class RestaurantsController < ApplicationController
  before_action :set_restaurant, only: [:show, :edit, :update, :destroy]

  def index
    @restaurants = Restaurant.all
  end

  def show
  end

  def new
    @restaurant = Restaurant.new
    @restaurant.build_location
  end

  def edit
  end

  def create
    @restaurant = Restaurant.new(restaurant_params)
    respond_to do |format|
      if @restaurant.save
        format.html { redirect_to restaurants_path, notice: "Created" }
      else
        format.html { redirect_to restaurants_path, alert: 'Creation failed' }
      end
    end
  end

  def update
    @restaurant = Restaurant.find(params[:id])
    respond_to do |format|
      if @restaurant.update(restaurant_params)
        format.html { redirect_to restaurants_path, notice: "Updated" }
      else
        format.html { redirect_to restaurants_path, alert: 'Creation failed' }
      end
    end
  end

  def destroy
    @restaurant.destroy
    redirect_to restaurants_path
  end

  private
  def set_restaurant
    @restaurant = Restaurant.find(params[:id])
  end

  def restaurant_params
    params.require(:restaurant).permit(:name, :description, location_attributes: [:country, :city, :address])
  end
end
