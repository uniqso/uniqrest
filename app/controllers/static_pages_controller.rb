class StaticPagesController < ApplicationController
  def home
    @restaurants = Restaurant.all
    @gmaps_hash  = Gmaps4rails.build_markers(@restaurants) do |restaurant, marker|
      marker.lat restaurant.location.latitude
      marker.lng restaurant.location.longitude
      marker.infowindow restaurant.name
    end
  end
end
