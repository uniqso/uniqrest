# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

init_one_marker_map = ($map) ->
  handler = Gmaps.build('Google')
  handler.buildMap { internal: id: 'one_marker' }, ->
    markers = handler.addMarkers([ {
      lat: $map.data('latitude')
      lng: $map.data('longitude')
      infowindow: $map.data('title')
    } ])
    handler.bounds.extendWith markers
    handler.fitMapToBounds()
    return

ready = ->
  $one_marker_map = $('#one_marker')
  init_one_marker_map($one_marker_map) if $one_marker_map.length > 0

$(document).ready ready
$(document).on 'page:load', ready