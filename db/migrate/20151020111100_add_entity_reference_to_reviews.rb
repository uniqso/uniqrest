class AddEntityReferenceToReviews < ActiveRecord::Migration
  def change
    add_reference :reviews, :entity, index: true, null: false, foreign_key: true
  end
end
