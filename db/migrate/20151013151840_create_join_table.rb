class CreateJoinTable < ActiveRecord::Migration
  def change
    create_join_table :entities, :criteria do |t|
      # t.index [:entity_id, :criterium_id]
      # t.index [:criterium_id, :entity_id]
      t.float :score, default: 0, null: false
    end
  end
end
