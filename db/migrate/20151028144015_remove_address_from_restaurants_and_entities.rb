class RemoveAddressFromRestaurantsAndEntities < ActiveRecord::Migration
  def up
    remove_column :restaurants, :address
    remove_column :entities, :address
  end
  def down
    add_column :restaurants, :address, :string
    add_column :entities, :address, :string
  end
end
