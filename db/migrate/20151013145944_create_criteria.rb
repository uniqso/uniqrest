class CreateCriteria < ActiveRecord::Migration
  def change
    create_table :criteria do |t|
      t.string :name
      t.references :entity_type, index: true, null: false, foreign_key: true
    end
  end
end
