class RemoveReviewIdFromEntities < ActiveRecord::Migration
  def up
    remove_column :entities, :review_id
  end
  def down
    add_reference :entities, :review, index: true, null: false, foreign_key: true
  end
end
