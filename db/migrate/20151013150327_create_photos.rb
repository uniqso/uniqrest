class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :url, null: false
      t.text :desription
      t.references :entity, index: true, null: false, foreign_key: true
    end
  end
end
