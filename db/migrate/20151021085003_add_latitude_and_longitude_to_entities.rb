class AddLatitudeAndLongitudeToEntities < ActiveRecord::Migration
  def change
    add_column :entities, :latitude, :float
    add_column :entities, :longitude, :float
  end
end
