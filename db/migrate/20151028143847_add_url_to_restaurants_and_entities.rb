class AddUrlToRestaurantsAndEntities < ActiveRecord::Migration
  def change
    add_column :restaurants, :url, :string
    add_column :entities, :url, :string
  end
end
