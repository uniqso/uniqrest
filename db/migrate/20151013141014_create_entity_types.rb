class CreateEntityTypes < ActiveRecord::Migration
  def up
    create_table :entity_types do |t|
      t.string :type,  null: false
    end
  end
  def down
    drop_table :entity_types
  end
end
