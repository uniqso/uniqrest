class CreateEntities < ActiveRecord::Migration
  def change
    create_table :entities do |t|
      t.references :entity_type, index: true, foreign_key: true
      t.string :name
      t.string :address
      t.references :review, index: true, null: false, foreign_key: true

      t.timestamps null: false
    end
  end
end
