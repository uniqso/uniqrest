class CreateLocations < ActiveRecord::Migration
  def change
    create_table :locations do |t|
      t.belongs_to :restaurant, index: true
      t.string :country
      t.string :city
      t.string :address
      t.float  :latitude
      t.float  :longitude

      t.timestamps null: false
    end
  end
end
