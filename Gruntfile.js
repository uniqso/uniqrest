module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      my_target: {
        options: {
          mangle: false,
          compress: false
        },
        files: {
          'public/js/output.js': [
            'frontend/vendor/jquery/jquery.min.js',
            'frontend/vendor/angular/angular.min.js',
'frontend/vendor/angular-route/angular-route.min.js',
'frontend/vendor/angular-resource/angular-resource.min.js',
'frontend/vendor/lodash/lodash.min.js',
'frontend/vendor/angular-google-maps/dist/angular-google-maps.min.js',
'frontend/vendor/angular-simple-logger/dist/angular-simple-logger.min.js',
            'frontend/js/**/*.js'
          ]
        }
      }
    },
    cssmin: {

      target: {
options: {
          mangle: false,
          compress: false
        },
        files: {
          'public/css/output.css': [
            'frontend/css/app.css',
'frontend/vendor/angular-material/angular-material.min.css',
'frontend/vendor/materialize/dist/css/materialize.min.css'
          ]
        }
      }
    },
    watch: {
      scripts: {
        files: ['frontend/js/**/*.js'],
        tasks: ['uglify']
      },
      css: {
        files: ['frontend/css/*.css'],
        tasks: ['cssmin']
      }
    }
   

  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Default task(s).
  grunt.registerTask('default', ['uglify:my_target', 'cssmin']);

};
