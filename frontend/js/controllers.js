'use strict';

/**
 * Created by greg on 15.11.15.
 */
var controllers = angular.module('controllers', ['ngResource']);

controllers.factory('location',function($resource){
    return $resource("test/locations.json")
});

controllers.directive('map', function ($compile, locationService, $timeout) {
    return function (scope, elem, attrs) {

        var mapOptions,
            latitude = attrs.latitude,
            longitude = attrs.longitude,
            divs,
            locations,
            map;
        $timeout(function(){
            locations = locationService.data;
            latitude = latitude && parseFloat(latitude, 10) || 46.4781688;
            longitude = longitude && parseFloat(longitude, 10) || 30.7138338;

            mapOptions = {
                zoom: 8,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    position: google.maps.ControlPosition.BOTTOM_CENTER
                },
                zoomControl: true,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                },
                center: new google.maps.LatLng(latitude, longitude)
            };
            map = new google.maps.Map(elem[0], mapOptions);

            function createMarker(location) {
                var marker = new google.maps.Marker({
                    position: {
                        lat: location.latitude,
                        lng: location.longitude
                    },
                    map: map,
                    title: location.name
                });
                return marker;
            }

            function createAllMarkers() {
                for (var i = 0; i < locations.length; i++) {
                    var marker = createMarker(locations[i]);
                    marker.setMap(map);
                }
            }

            divs = document.getElementById('divs').innerHTML.trim();
            divs = $compile(divs)(scope);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(divs[0]);
            locationService.map = map;
            createAllMarkers();
        },250);


    };

});

controllers.controller("searchLstCtrl", function ($scope, locationService) {

    console.log(locationService.data);
    $scope.search = "";
    $scope.locations = locationService;

    //searches for location with similar name,tag, description
    $scope.searcher = function (location) {
        if ($scope.search) {
            return location.name.indexOf($scope.search.toLowerCase()) == 0 ||
                location.tag.indexOf($scope.search.toLowerCase()) == 0 ||
                location.desc.indexOf($scope.search.toLowerCase()) == 0;
        }
        return true;
    };

    $scope.chooseLocation = function (location) {
        locationService.selectedLocation = location;
        locationService.map.setCenter({lat: location.latitude,
            lng: location.longitude});
    };


});


controllers.service('locationService', function (location) {

    var result = {
        'map': null,
        'selectedLocation': null,
        'data': [],
        'getLocations': function(){
            location.get(function(response){
                console.log(response);
                angular.forEach(response.data,function(loc){
                    result.data.push(new location(loc));
                })
            });
        }
    };
    result.getLocations();
    return result;
});
